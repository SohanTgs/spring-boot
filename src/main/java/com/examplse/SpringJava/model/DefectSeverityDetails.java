package com.examplse.SpringJava.model;

public class DefectSeverityDetails {

	private String high;
	private String low;
	private String medium;
	
	public String getHigh() {
		return high;
	}
	public void setHigh(String high) {
		this.high = high;
	}
	public String getLow() {
		return low;
	}
	public void setLow(String low) {
		this.low = low;
	}
	public String getMedium() {
		return medium;
	}
	public void setMedium(String medium) {
		this.medium = medium;
	}

	
	

}
